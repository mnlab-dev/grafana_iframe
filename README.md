
# Embed grafana dashboard in an app using iframe

Sharing a grafana dashboard with an iframe inside your custom app has the problem that the user may be asked for a second login (grafana allows access only to authorized users). To integrate grafana in a smoother way, the following work-around steps are proposed. The general idea is:

 1. load the login page of grafana inside an iframe (which is not even displayed)
 2. use javascript to insert username/password to the appropriate fields
 3. trigger a click event to the submit button
 4. after having logged in, share the dashboard you want without asking the user for authentication


## Setup: put Grafana and your app behind a reverse-proxy

This will help overcome the `cross-origin` problems. Adding an iframe with grafana running on a different host, will not let the browser control the iframe's DOM. In our test case, we have:

 - a tiny Go server to serve `index.html`, running on port 4545 `(http://localhost:4545)`
 - Grafana, running on the default port 3000 `(http://localhost:3000)`
 
 After we configure the reverse-proxy:
 
```
http://localhost/          : returns the index.html.
http://localhost/grafana/  : returns the grafana home page.
```

### Nginx installation

```
sudo apt-get update
sudo apt-get install nginx

```

### Nginx configuration `/etc/nginx/nginx.conf`


```
server {
  listen 80 default_server;

  location /grafana/ {
    proxy_pass http://localhost:3000/; # if not on localost, use the IP of the grafana server...
  }

  location / {
    proxy_pass http://localhost:4545/; # change if your app uses a different port...
  }
}
```


### Nginx configuration 2 (disable default site)

Open file: `/etc/nginx/sites-available/default` and comment out the `server` part.

```
server {
 ...
}
```

To restart the service after modifying the files:

```
sudo systemctl stop nginx.service
sudo systemctl start nginx.service
```

 
### Grafana configuration `/etc/grafana/grafana.ini`
 
Spot the relevant lines and use the following:
 
```
domain = localhost
root_url = http://localhost/grafana/
```

To restart the service after modifying the files:

```
sudo systemctl stop grafana-server.service
sudo systemctl start grafana-server.service
```

## How to launch the Go server

The used port is `4545` but you can modify it inside `server.go`. To run the server just type `go run server.go`.


## Prepare your test

If your setup is slightly different, you may have to change some javascript variables inside `index.html`. There is a section with all the global vars, like this:

```javascript
var grafana_url         = "http://localhost/grafana/";
var grafana_dashboard   = "http://localhost/grafana/dashboard/db/sesame-node-exporter?refresh=10s&orgId=1" // our test dashboard
var grafana_user        = "admin";
var grafana_pass        = "admin";
```



## Useful links

 - https://github.com/grafana/grafana/issues/5783
 - https://github.com/grafana/grafana/issues/3752
 - https://stackoverflow.com/questions/33795843/grafana-passing-access-token-in-url
 - https://stackoverflow.com/questions/13432821/is-it-possible-to-add-request-headers-to-an-iframe-src-request
 - https://stackoverflow.com/questions/45836514/no-response-from-grafana-via-ajax
 - https://stackoverflow.com/questions/17694807/how-to-set-custom-http-headers-when-changing-iframe-src
 - https://stackoverflow.com/questions/42303800/how-to-display-grafana-graphs-in-my-websites-admin-panel-securely
 - https://stackoverflow.com/questions/40522499/grafana-web-visual-read-only-kiosk-mode-for-integration-in-enteprise-app
 - http://www.3till7.net/2014/08/09/triggering-events-in-an-iframe/index.html

